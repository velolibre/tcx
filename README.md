# Goals

  1. [x] build a TCX (de)serializer that's good enough for the kinds of files I personally need to record/merge
  2. gather other example files
  3. draw from the official spec and implement the whole thing

In all cases, the ability to serialize and deserialize losslessly is *critical*. I consider clean round-tripping preferable, but not absolutely required in all cases, such as minor ordering, formatting, and annotation issues.
