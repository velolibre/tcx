use tcx;

use std::fs;
use std::io::{BufReader};

#[test]
fn simple() {
    let mut input = BufReader::new(fs::File::open("tests/simple.tcx").unwrap());
    let in_buf = fs::read("tests/simple.tcx").unwrap();

    let mut out_buf = Vec::new();

    tcx::read(&mut input).and_then(|mut db| Ok(tcx::write(&mut out_buf, &mut db))).unwrap();
    //fs::write("tests/debug.tcx", out_buf).unwrap();
    assert_eq!(out_buf, in_buf);
}
