use std::io;
use chrono::{DateTime, Utc};
use yaserde_derive::{YaSerialize, YaDeserialize};
use yaserde::{de,ser};

#[derive(Debug, PartialEq, YaSerialize, YaDeserialize, Default)]
#[yaserde(
    root = "TrainingCenterDatabase",
    prefix = "ns",
    default_namespace = "ns",
    namespace = "ns: http://www.garmin.com/xmlschemas/TrainingCenterDatabase/v2")]
pub struct TrainingCenterDatabase {
    #[yaserde(rename = "Activities")]
    pub activities: Activities
}

#[derive(Debug, PartialEq, YaSerialize, YaDeserialize, Default)]
pub struct Activities {
    #[yaserde(rename = "Activity")]
    pub activity: Vec<Activity>
}

#[derive(Debug, PartialEq, YaSerialize, YaDeserialize, Default)]
#[yaserde(
    prefix = "ns",
    default_namespace = "ns",
    namespace = "ns: http://www.garmin.com/xmlschemas/TrainingCenterDatabase/v2")]
pub struct Activity {
    #[yaserde(rename = "Id", prefix = "ns")]
    pub id: String,
    #[yaserde(rename = "Lap")]
    pub lap: Lap, // TODO vec
    #[yaserde(attribute, rename = "Sport")]
    pub sport: String,
}

#[derive(Debug, PartialEq, YaSerialize, YaDeserialize, Default)]
#[yaserde(
    prefix = "ns",
    default_namespace = "ns",
    namespace = "ns: http://www.garmin.com/xmlschemas/TrainingCenterDatabase/v2")]
pub struct Lap {
    #[yaserde(attribute, rename = "StartTime")]
    pub start_time: String,
    #[yaserde(rename = "TotalTimeSeconds", prefix = "ns")]
    pub total_time_seconds: f32,
    #[yaserde(rename = "DistanceMeters", prefix = "ns")]
    pub distance_meters: f32,
    #[yaserde(rename = "MaximumSpeed", prefix = "ns")]
    pub maximum_speed: f32,
    #[yaserde(rename = "Calories", prefix = "ns")]
    pub calories: u32,
    #[yaserde(rename = "AverageHeartRateBpm")]
    pub average_heart_rate_bpm: Option<HR>,
    #[yaserde(rename = "MaximumHeartRateBpm")]
    pub maximum_heart_rate_bpm: Option<HR>,
    #[yaserde(rename = "Cadence", prefix = "ns")]
    pub cadence: u32,
    #[yaserde(rename = "Intensity", prefix = "ns")]
    pub intensity: String,
    #[yaserde(rename = "TriggerMethod", prefix = "ns")]
    pub trigger_method: String,
    #[yaserde(rename = "Track")]
    pub track: Track,
}

#[derive(Debug, PartialEq, YaSerialize, YaDeserialize, Default)]
pub struct Track {
    #[yaserde(rename = "Trackpoint")]
    pub trackpoint: Vec<TrackPoint>
}

#[derive(Debug, PartialEq, YaSerialize, YaDeserialize, Default)]
#[yaserde(
    prefix = "ns",
    default_namespace = "ns",
    namespace = "ns: http://www.garmin.com/xmlschemas/TrainingCenterDatabase/v2")]
pub struct Position {
    #[yaserde(rename = "LatitudeDegrees", prefix = "ns")]
    pub latitude_degrees: f64,
    #[yaserde(rename = "LongitudeDegrees", prefix = "ns")]
    pub longitude_degrees: f64,
}

#[derive(Debug, PartialEq, YaSerialize, YaDeserialize, Default)]
pub struct Extensions {
    #[yaserde(rename = "TPX")]
    pub tpx: TPX
}

#[derive(Debug, PartialEq, YaSerialize, YaDeserialize, Default)]
#[yaserde(namespace = "http://www.garmin.com/xmlschemas/ActivityExtension/v2")]
pub struct TPX {
    #[yaserde(rename = "Watts")]
    pub watts: u32,
}

#[derive(Debug, PartialEq, YaSerialize, YaDeserialize, Default)]
#[yaserde(
    prefix = "ns",
    default_namespace = "ns",
    namespace = "ns: http://www.garmin.com/xmlschemas/TrainingCenterDatabase/v2")]
pub struct HR {
    #[yaserde(rename = "Value", prefix = "ns")]
    pub value: u16,
}

#[derive(Debug, PartialEq, YaSerialize, YaDeserialize, Default)]
#[yaserde(
    prefix = "ns",
    default_namespace = "ns",
    namespace = "ns: http://www.garmin.com/xmlschemas/TrainingCenterDatabase/v2")]
pub struct TrackPoint {
    #[yaserde(rename = "Time", prefix = "ns")]
    pub time: Option<String>,
    #[yaserde(rename = "Position")]
    pub position: Option<Position>,
    #[yaserde(rename = "AltitudeMeters", prefix = "ns")]
    pub altitude_meters: Option<String>,
    #[yaserde(rename = "DistanceMeters", prefix = "ns")]
    pub distance_meters: Option<String>,
    #[yaserde(rename = "HeartRateBpm")]
    pub heart_rate_bpm: Option<HR>,
    #[yaserde(rename = "SensorState", prefix = "ns")]
    pub sensor_state: Option<String>,
    #[yaserde(rename = "Cadence", prefix = "ns")]
    pub cadence: Option<u32>,
    #[yaserde(rename = "Extensions")]
    pub extensions: Option<Extensions>,
}

pub fn read(stream: &mut impl io::Read) -> Result<TrainingCenterDatabase, String> {
    let result: Result<TrainingCenterDatabase, _> = de::from_reader(stream);
    return result;
}

pub fn write(buf: &mut impl io::Write, db: &mut TrainingCenterDatabase) {
    ser::serialize_with_writer(db, buf, &ser::Config{perform_indent: true, write_document_declaration: true, indent_string: None}).unwrap();
}
